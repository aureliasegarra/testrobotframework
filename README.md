[![Front](https://img.shields.io/badge/Test-RobotFramework-green?style=flat)](https://)
[![Front](https://img.shields.io/badge/TestCase-LoginPage-blue?style=flat)](https://)
[![Front](https://img.shields.io/badge/Development-DataDrivenTesting-pink?style=flat)](https://)

<br/>

<div align="center">
<img src="loginForm3D.png" alt="Login Form" width="25%">

<br/>
<br/>
<h1 align="center">RobotFramework</h1>
<p align="center">Connection tests on booking website</p>
</div>


<br/>
<br/>

<!-- TABLE OF CONTENTS -->

<details open="open">
<summary>Table of Contents</summary>

<ol>
    <li><a href="#illustration">Illustration</a></li>
    <li><a href="#description">Description</a></li>
    <li><a href="#languages">Languages & tools</a></li>
    <li><a href="#objectives">Objectives</a></li>
    <li><a href="#setup">SetUp</a></li>
    <li><a href="#status">Status</a></li>
    <li><a href="#context">Context</a></li>
</ol>

</details>

<br>
<br>

## ✨ Illustration <a id="illustration"></a>

![illustration](screenshot.png)

## 🗒 Description <a id="description"></a>

Test cases to test login connection for a booking website

System Under Testing (SUT) : http://livraison3.testacademy.fr

## 🛠 Languages/tools <a id="languages"></a>

* Python
* RobotFramework
* SeleniumLibrary
* Selenium webdriver
* WebDriverManager
  
## 🎯 Objectives <a id="objectives"></a>

* Discover RobotFramework
* Improve main features (Syntax, Settings, Keywords, Variables, Setup, Teardown, Template)
* Improve Data Driven Testing

## ⚙️ SetUp <a id="setup"></a>

How to launch RobotFramework tests :
```
robot testrobotframework/connexions-valides.robot
```

## 📈 Status <a id="status"></a>
Project completed

## 🗓 Context <a id="context"> </a>

I realized this practical work during my bachelor of computer science in the Institute of technology of Vannes
